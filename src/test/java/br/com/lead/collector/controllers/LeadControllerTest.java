package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Alder");
        lead.setEmail("Alder@teste.com");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setDescricao("Café do Pelé");
        produto.setValor(20.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);

        //Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto ->
        //{
        //    lead.setId(1);
        //    return lead;
        //});

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));
    }

    @Test
    public void testarExibirTodos() throws Exception {

        Iterable<Lead> leadIterable = new ArrayList<>();
        Mockito.when(leadService.buscarTodos()).thenReturn(leadIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarBuscarPorId() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/{id}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarAtualizarLead()throws Exception{

        Mockito.when(leadService.atualizarLead(Mockito.anyInt(), lead)).thenReturn(lead);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/{id}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(lead.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo(lead.getEmail())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(lead.getData())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.tipoLead", CoreMatchers.equalTo(TipoLeadEnum.FRIO)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos", CoreMatchers.equalTo(lead.getProdutos())));
    }


}
