package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    TipoLeadEnum tipoLeadEnum;
    Iterable<Lead> leads;
    Optional<Lead> optionalLead;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Alder");
        lead.setEmail("Alder@teste.com");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setDescricao("Café do Pelé");
        produto.setValor(20.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);

    }

    @Test
    public void testarBuscarPorTodosLeads() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarSalvarLead() {

        Mockito.when(produtoService.buscarPorTodosId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Lead Teste");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("LeadTeste@teste.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadTeste);

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));

    }

    @Test
    public void testarBuscarTodosPorTipoLead() {

        Mockito.when(leadRepository.findAllByTipoLead(tipoLeadEnum)).thenReturn(leads);

        Iterable<Lead> retorno = leadService.buscarTodosPorTipoLead(tipoLeadEnum);

        Assertions.assertEquals(leads, retorno);

    }

    @Test
    public void testarBuscarPorId() {
        int id = 1;

        Mockito.when(leadRepository.findById(id)).thenReturn(optionalLead);

        Lead leadRetorno = leadService.buscarPorId(id);

        Assertions.assertEquals(optionalLead, leadRetorno);
    }

    @Test
    public void testarBuscarPorIdNaoEncontrado() {

        Mockito.when(leadRepository.findById(1)).thenReturn(optionalLead);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.buscarPorId(1);
        });
    }

    @Test
    public void testarAtualizarLead() {

        Mockito.when(leadRepository.existsById(1)).thenReturn(true);

        Lead leadTeste = new Lead();
        leadTeste.setId(1);
        leadTeste.setNome("Lead Teste");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("LeadTeste@teste.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        leadService.atualizarLead(1, leadTeste);

        Mockito.verify(leadRepository, Mockito.times(1)).existsById(leadTeste.getId());
    }

    @Test
    public void testarAtualizarLeadNaoEncontrado(){
        Mockito.when(leadRepository.existsById(1)).thenReturn(false);

        Lead leadTeste = new Lead();
        leadTeste.setId(1);
        leadTeste.setNome("Lead Teste");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("LeadTeste@teste.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.atualizarLead(1, leadTeste);
        });
    }

    @Test
    public void testarDeletarLead() {

        Mockito.when(leadRepository.existsById(1)).thenReturn(true);

        leadService.deletarLead(1);

        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(1);

    }

    @Test
    public void testarDeletarLeadNaoEncontrado() {

        Mockito.when(leadRepository.existsById(1)).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.deletarLead(1);
        });
    }
}
