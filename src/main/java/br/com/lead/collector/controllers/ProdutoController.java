package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    public ProdutoService produtoService;

    @PostMapping
    public Produto incluirProduto(@RequestBody Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(@RequestParam(name = "Nome", required = false) String nome) {
        if (nome != null) {
            Iterable<Produto> produtos = produtoService.buscarProdutoPorNome(nome);
            return produtos;
        }
        Iterable<Produto> produtosObjeto = produtoService.buscarTodosProdutos();
        return produtosObjeto;
    }

    @GetMapping("/{id}")
    public Produto buscarProdutoPorId(@PathVariable(name = "id") int id) {
        try {
            Produto produto = produtoService.buscarProdutoPorId(id);
            return produto;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") int id, @RequestBody Produto produto) {
        try {
            Produto produtoObjeto = produtoService.atualizarProduto(id, produto);
            return produtoObjeto;
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable(name = "id") int id) {
        try {
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
