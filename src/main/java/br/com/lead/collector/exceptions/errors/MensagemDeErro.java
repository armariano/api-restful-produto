package br.com.lead.collector.exceptions.errors;

import org.springframework.validation.ObjectError;

import java.util.HashMap;

public class MensagemDeErro {
    private String error;
    private String mensagemDeErro;
    private HashMap<String, ObjetoDeErro> camposDeErro;

    public MensagemDeErro() {
    }

    public MensagemDeErro(String error, String mensagemDeErro, HashMap<String, ObjetoDeErro> camposDeErro) {
        this.error = error;
        this.mensagemDeErro = mensagemDeErro;
        this.camposDeErro = camposDeErro;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public HashMap<String, ObjetoDeErro> getCamposDeErro() {
        return camposDeErro;
    }

    public void setCamposDeErro(HashMap<String, ObjetoDeErro> camposDeErro) {
        this.camposDeErro = camposDeErro;
    }
}
