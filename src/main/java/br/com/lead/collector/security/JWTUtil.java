package br.com.lead.collector.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    @Value("${jwt.segredo}")
    private String segredo;

    @Value("${jwt.expiracao}")
    private long expiracao;

    public String gerarToken(String userName) {
        String token = Jwts.builder().setSubject(userName)
                .setExpiration(new Date(System.currentTimeMillis() + expiracao))
                .signWith(SignatureAlgorithm.HS512, segredo.getBytes())
                .compact();
        return token;
    }

    //Configuração de reconhecimento do Token

    public boolean tokenValido(String token) {

        try {
            Claims claims = getClaims(token);

            String email = claims.getSubject();

            Date dataExpiracao = claims.getExpiration();
            Date dataAtual = new Date(System.currentTimeMillis());

            if (email != null && dataExpiracao != null && dataAtual.before(dataExpiracao)) {
                return true;
            } else {
                return false;
            }


        } catch (RuntimeException e) {
            return false;
        }
    }

    public Claims getClaims(String token) {

        try {
            return Jwts.parser().setSigningKey(segredo.getBytes()).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            throw new RuntimeException();
        }

    }

    public String getUsername(String token) {
        Claims claims = getClaims(token);
        String username = claims.getSubject();
        return username;
    }
}
