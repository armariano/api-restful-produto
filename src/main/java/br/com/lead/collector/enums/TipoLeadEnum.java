package br.com.lead.collector.enums;

public enum TipoLeadEnum {
    QUENTE,
    FRIO,
    ORGANICO
}
