package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> buscarTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Iterable<Produto> buscarProdutoPorNome(String nome){
        return produtoRepository.findAllByNome(nome);
    }

    public Produto buscarProdutoPorId(int id){
        Optional<Produto> produto = produtoRepository.findById(id);
        if(produto.isPresent()){
            return produto.get();
        }
        throw new RuntimeException("Produto não encontrado");
    }

    public Produto atualizarProduto(int id, Produto produto) {
        if (produtoRepository.existsById(id)) {
            produto.setId(id);
            Produto produtoObjeto = salvarProduto(produto);

            return produtoObjeto;
        }
        throw new RuntimeException("Produto não encontrado");
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public List<Produto> buscarPorTodosId(List<Integer> id){
        Iterable<Produto> produto = produtoRepository.findAllById(id);
        return (List)produto;
    }
}
